const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const constructionDetailSchema = new Schema({
    
    name: {
      type: String
    },
    address: {
        name: {
            type: String
        },
        lai: {
            type: Number
        },
        long: {
            type: Number
        }
    },
    id_image: {
        type: Number,
    },
    id_construction: {
        type: Number
    },
    flag: {
        type: Boolean,
        default: true
    },
    created_at: { type: Date, default: Date.now },
});

const ConstructionDetail = mongoose.model('constructionDetail', constructionDetailSchema);
module.exports = ConstructionDetail;
