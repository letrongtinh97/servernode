const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    
    name: {
      type: String
    },
    username: {
        type: String,
    },
    password: {
        type: String,
    },
    id_fb: {
        type: Number
    },
    avatar: {
        type: String
    },
    flag: {
        type: Boolean,
        default: true
    },
    created_at: { type: Date, default: Date.now },
});

const User = mongoose.model('users', userSchema);
module.exports = User;
