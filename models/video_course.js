const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schemaVideoCourse = new Schema({
    created: {
        type: Date, default: Date.now()
    },
    updated: { type: Date, default: Date.now },
    link: {
        part: {
            type: Number
        },
        sub_part: {
          type: String
        },
        link: {
            type: String
        },
        flag: {
            type: Boolean,
            default: true
        }
    },
    name: {
        type: String
    },
    id_course: {
        type: Number
    },
    thumbnail: {
        type: String
    },
    flag: {
        type: Boolean,
        default: true
    }
});

const VideoCourse = mongoose.model('video_course', schemaVideoCourse);
module.exports = VideoCourse;
