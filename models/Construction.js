const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const constructionSchema = new Schema({
    
    name: {
      type: String
    },
    title: {
        type: String,
    },
    id_user: {
        type: String,
    },
    flag: {
        type: Boolean,
        default: true
    },
    created_at: { type: Date, default: Date.now },
});

const Construction = mongoose.model('constructions', constructionSchema);
module.exports = Construction;
