const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schemaTimLine = new Schema({
    id_user: {
      type: Number,
    },
    id_type: {
        type: Number,
    },
    name: {
        type: String,
    },
    title: {
        type: String,
    },
    image: {
        type: String,
    },
    payment_type: {
        type: Number,
        default: 0
    },
    rank: {
        type: Number,
        min: 0,
        max:21,
        default:0
    },
    flag: {
        type: Boolean,
        default:true
    },
    updated: { type: Date, default: Date.now },

});

const TimeLine = mongoose.model('timeline', schemaTimLine);
module.exports = TimeLine;
