const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schemaPayment = new Schema({
    payment_type: {
        type: Number
    },
    id_user: {
        type: Number
    },
    total: {
      type: Number
    },
    id_payment: {
        type: Number
    },
    flag: {
        type: Boolean,
        default: true
    },
    updated: { type: Date, default: Date.now },
})

const Payment = mongoose.model('timeline', schemaPayment);
module.exports = Payment;
