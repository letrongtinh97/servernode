const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schemaCourse = new Schema({
    id_type: {
      type: Number
    },
    name: {
      type: String
    },
    title: {
        type: String,
    },
    auth: {
        main_auth: {
            type: String
        },
        sub_auth: {
            type: String
        }
    },
    rank: {
        type: Number
    },
    updated: { type: Date, default: Date.now },
});

const Course = mongoose.model('course', schemaCourse);
module.exports = Course;
