const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const addressSchema = new Schema({
    
    title: {
        type: String
    },
    name: {
        type: String,
    },
    coord: {
        lai: {
            type: Number
        },
        long: {
            type: Number
        }
    },
    id_works: {
        type: String
    },
    id_user: {
        type: String,
    },
    flag: {
        type: Boolean,
        default: true
    },
    created_at: { type: Date, default: Date.now },
});

const Address = mongoose.model('address', addressSchema);
module.exports = Address;
