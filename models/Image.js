const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const imagesSchema = new Schema({
    
    url: {
      type: String
    },
    id_construction_detail: {
        type: String,
    },
    flag: {
        type: Boolean,
        default: true
    },
    created_at: { type: Date, default: Date.now },
});

const ImageS = mongoose.model('images', imagesSchema);
module.exports = ImageS;
