var express = require('express');
var router = express.Router();
const Ninja = require('../models/db')
const TimeLine = require('../models/TimeLine')
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/posts', (req, res, next) => {
  if(!req.body){
    return res.status().json({
      code: 2 // no data
    })
  }
  TimeLine.create(req.body)
      .then(function (ninja) {
        return res.status(200).json({
        code: 0,//success
        data: ninja
      })
      }).catch((e) =>{
        return res.status(500).json({
          code: 1//no create data
        })
      })
})
module.exports = router;
