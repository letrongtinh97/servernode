const express = require('express');
const router = express.Router();

const Course = require('../models/Course');

//get all
router.get('/', function(req, res, next) {
    //all
    Course.find({})
        .then((rs)=>{
         return res.status(200).json({
            code: 0,
            data: rs
        })
    })
        .catch((e)=>{
        return res.status(500).json({
            code: 1
        })
    })
});

router.post('/', (req,res) => {
    const data = req.body
    if(!data){
        return res.status(500).json({
            code: 2
        })
    }
    Course.create(data)
        .then((rs)=>{
        return res.status(200).json({
            code: 0,
            data: rs
        })
    })
        .catch((e)=> {
        return res.status(500).json({
            code: 1
        })
    })
})
module.exports = router;
