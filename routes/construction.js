const express = require('express');
const router = express.Router();

const Construction = require('../models/Construction');

//get all


//get TimeLine by id_type

router.post('/', (req,res) => {
    const data = req.body
    if(!data){
        return res.status(500).json({
            code: 2
        })
    }
    Construction.create(data).then((rs)=>{
        res.status(200).json({
            code: 0,
            data: rs
        })
    }).catch((e)=> {
        res.status(500).json({
            code: 1
        })
    })
})

router.get('/', (req,res,next) => {
   
    const query = {flag: true}
    Construction.find(query).then((rs)=>{
        if(rs.length === 0){
            return res.status(200).json({
                code: 3,
                message: 'no construction'
            })
        }
        return res.status(200).json({
            code: 0,
            data: rs,
            message: 'success'
        })
    }).catch((e)=> {
        res.status(500).json({
            code: 1,
            message: 'no construction'
        })
    })
})


module.exports = router;
