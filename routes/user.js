const express = require('express');
const router = express.Router();
const {decrypt, encrypt} = require('../libs/crypto');
const User = require('../models/User');

//get all


//get TimeLine by id_type
router.post('/create-address',(req,res) => {
    
});

router.post('/update-address-works')
router.post('/user-facebook', (req,res) => {
    const {id,name,picture} = req.body
    let data = {
        id_fb: id ? id : 0,
        avatar: picture && picture.data && picture.data.url ? picture.data.url : '',
        name: name ? name : '',
    }
    if(!id){
        return res.status(200).json({
            code: 10003,
            message: 'no find id',
        })
    }

    User.findOne({id_fb: id}).then((rs)=>{
        if(rs){
            return res.status(200).json({
                code: 10001,
                data: rs,
                message: 'find a user'
            })
        }
        User.create(data).then((rs1)=>{
            if(rs1){
                return res.status(200).json({
                    code: 10002,
                    data: rs1,
                    message: 'create success'
                })
            }
        }).catch((err)=>{
            return res.status(500).json({
                code: 1,
                data: null,
                message: 'create fail'
            })
        })
    }).catch((e)=> {
        res.status(500).json({
            code: 1,
            data: null,
            message: 'create fail'
        })
    })
})

router.post('/create',(req,res,next) => {
    const {username,name,password} = req.body
    console.log(req.body)
    if(!username || !password){
        return res.status(200).json({
            code: 40004,
            message:'Not Found'
        })
    }
    let data = {
        username: username,
        password: password,
        name: name ,
    }
    User.findOne({username:username}).then((result)=>{
        if(result !== null){
            return res.status(200).json({
                code: 2,
                message: 'exist user'
            })
        }
        User.create(data).then((rs)=>{
            return res.status(200).json({
                code: 0,
                data: rs,
                message: 'create success'
            })
        }).catch((e)=>{
            return res.status(500).json({
                code: 1,
                data: [],
                message: 'sever error'
            })
        })
    }).catch((er)=>{
        return res.status(500).json({
            code: 1,
            data: [],
            message: 'sever error'
        })
    })
});

router.post('/get-user', (req,res,next) => {
    const {username, password} = req.body
    console.log(req.body)
    if(!username || !password){
        return res.status(200).json({
            code: 2,
            message: 'exist user'
        })
    }
    let data = {
        username: username
    }
    User.findOne(data).then((rs)=>{
        if(rs === null){
            return res.status(200).json({
                code: 3})
        }
        console.log(decrypt(rs.password))
        const ec_password = decrypt(rs.password)

        if(ec_password !== password){
            return res.status(200).json({
                code: 20001,
                message: 'not match password'
            })
        }

        return res.status(200).json({
            code: 0,
            data: rs,
            message: 'Success'
        })
    }).catch((e)=> {
        res.status(500).json({
            code: 1
        })
    })
})
module.exports = router;
