const express = require('express');
const router = express.Router();

const TimeLine = require('../models/TimeLine');
router.get('/test', (req, res)=> {
    res.status(200).json({
        code: 1
    })
})
//get all
router.get('/', function(req, res, next) {
    //all
    TimeLine.find({}).then((rs)=>{
        res.status(200).json({
            code: 0,
            data: rs
        })
    }).catch((e)=>{
        res.status(500).json({
            code: 1
        })
    })

    //get near index
    // TimeLine.geoNear(
    //     {type: }
    // )
});

//get TimeLine by id_type
router.get('/:id', (req, res) => {
    const data = req.params.id
    TimeLine.find({id_type:data}).then((rs)=>{
        if(rs.length === 0){
            return res.status(200).json({
                code: 3,
                message: 'select data null'
            })
        }
        return res.status(200).json({
            code: 0,
            data:rs
        })
    }).catch((e)=>{
        return res.status(500).json({
            code: 1
        })
    })

})

router.post('/', (req,res) => {
    const data = req.body
    if(!data){
        return res.status(500).json({
            code: 2
        })
    }
    TimeLine.create(data).then((rs)=>{
        res.status(200).json({
            code: 0,
            data: rs
        })
    }).catch((e)=> {
        res.status(500).json({
            code: 1
        })
    })
})
module.exports = router;
