var CryptoJS = require("crypto-js");
const SECRET_KEY = 'test';
// Encrypt
const encrypt = (text) => {
    let ciphertext = CryptoJS.AES.encrypt(text, SECRET_KEY).toString();
    return ciphertext;
}
// Decrypt

const decrypt = (text) => {
    const bytes  = CryptoJS.AES.decrypt(text, SECRET_KEY);
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
}
//console.log(originalText);

module.exports = {
    encrypt,
    decrypt
}