var createError = require('http-errors');
var express = require('express');
var path = require('path');
const http = require('http')
const mongoose = require('mongoose');
const socket = require('socket.io')
var logger = require('morgan');
var bodyParser = require('body-parser')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const timelineRouter = require('./routes/timeline');
const userRouter = require('./routes/user');
const constructionRouter = require('./routes/construction'); 
const addressRouter = require('./routes/address')
let app = express();
let server = http.createServer(app);
let io =socket(server)
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


mongoose.connect('mongodb+srv://tinhle:123123a@cluster0-uvlcl.mongodb.net/ECDMaps', {
    useNewUrlParser: true, useUnifiedTopology: true});
mongoose.Promise = global.Promise;


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/timeline', timelineRouter);
app.use('/user',userRouter)
app.use('/cons',constructionRouter);
app.use('/address',addressRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.listen(4000,()=>{
    console.log('server running port 4000 ')
})
